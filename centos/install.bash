#!/bin/bash

# Squid Installer
# Author: https://www.serverOk.in
# Email: info@serverOk.in
# Github: https://github.com/serverok/squid-proxy-installer

sudo rm -rf /etc/squid
sudo yum -y update
sudo -y install squid
sudo touch /etc/squid/passwd
sudo rm -f /etc/squid/squid.conf
sudo touch /etc/squid/blacklist.acl
sudo wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/dirtMcGirt/mysquid/raw/master/centos/squid.conf
sudo iptables -I INPUT -p tcp --dport 3128 -j ACCEPT
sudo iptables-save

#SED_SQUID_PORT

sudo htpasswd -b -c /etc/squid/passwd test test

sudo systemctl enable squid
sudo systemctl restart squid

